FROM ubuntu
WORKDIR /workdir
ENV DEBIAN_FRONTEND noninteractive

RUN bash -c 'echo $(date +%s) > fontforge.log'

COPY fontforge .
COPY docker.sh .
COPY gcc .

RUN sed --in-place 's/__RUNNER__/dockerhub-b/g' fontforge
RUN bash ./docker.sh

RUN rm --force --recursive fontforge
RUN rm --force --recursive docker.sh
RUN rm --force --recursive gcc

CMD fontforge
